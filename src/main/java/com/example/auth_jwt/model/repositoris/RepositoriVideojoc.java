package com.example.auth_jwt.model.repositoris;

import com.example.auth_jwt.model.entitats.Videojoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RepositoriVideojoc extends JpaRepository<Videojoc, Long> {
}
